module.exports = {
	apps: [{
		name: "hemsida",
		script: "dist/src/index.js",
		watch: ["dist/"],
		env: {
			"PORT": 3000,
			"NODE_ENV": "development",
			"DOMAIN": "localhost",
			"API_DOMAIN": "localhost"
		},
		env_production: {
			"PORT": 3000,
			"NODE_ENV": "production",
			"DOMAIN": "felix.ringberg.net",
			"API_DOMAIN": "api.ringberg.net"
		}
	}]
};
