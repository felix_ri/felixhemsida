module.exports = {
	"root": true,
	"env": {
		"es6": true,
		"node": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended"
	],
	"globals": {
		"Atomics": "readonly",
		"SharedArrayBuffer": "readonly"
	},
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"ecmaVersion": 2020,
		"sourceType": "module"
	},
	"plugins": [
		"@typescript-eslint",
		"simple-import-sort"
	],
	"rules": {
		"indent": [
			"warn",
			"tab",
			{
				"SwitchCase": 1
			}
		],
		"linebreak-style": [
			"warn",
			"unix"
		],
		"quotes": [
			"warn",
			"double"
		],
		"semi": [
			"warn",
			"always"
		],
		"no-console": "warn",
		"no-empty": "warn",
		"no-unused-vars": "warn",
		"sort-imports": "off",
		"import/order": "off",
		"simple-import-sort/imports": "warn",
		"simple-import-sort/exports": "warn"
	}
};