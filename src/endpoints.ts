import { ProductEndpoint } from "./endpoints/api/ProductEndpoint";
import { ProductsEndpoint } from "./endpoints/api/ProductsEndpoint";
import { Endpoint } from "./endpoints/Endpoint";
import { EndpointMap } from "./endpoints/types";
import { AboutViewEndpoint } from "./endpoints/view/AboutViewEndpoint";
import { IndexViewEndpoint } from "./endpoints/view/IndexViewEndpoint";
import { ProductsViewEndpoint } from "./endpoints/view/ProductsViewEndpoint";
import { ProductViewEndpoint } from "./endpoints/view/ProductViewEndpoint";
import { TestViewEndpoint } from "./endpoints/view/TestViewEndpoint";
import { Context } from "./types";

export async function populateEndpoints(context: Context): Promise<void> {
	const viewEndpoints: EndpointMap = {
		"/index/view": new IndexViewEndpoint(),
		"/about/view": new AboutViewEndpoint(),
		"/products/view": new ProductsViewEndpoint(),
		"/products/:productId/view": new ProductViewEndpoint(),
		"/test/view": new TestViewEndpoint()
	};

	const apiEndpoints: EndpointMap = {
		"/products": new ProductsEndpoint(context.log),
		"/products/:productId": new ProductEndpoint(context.log)
	};

	for (const [endpoint, endpointClass] of Object.entries(viewEndpoints)) {
		await populateViewEndpoint(context, endpoint, endpointClass);
	}

	for (const [endpoint, endpointClass] of Object.entries(apiEndpoints)) {
		await populateApiEndpoint(context, endpoint, endpointClass);
	}
}

async function populateApiEndpoint(context: Context, endpoint: string, endpointObject: Endpoint): Promise<void> {
	const constraints = context.environment === "production" ? { host: context.apiDomain } : undefined;

	await context.log?.info(`Populating API endpoint "${endpoint}" with endpointObject ${endpointObject.name}`);

	context.server.route({
		method: "GET",
		url: endpoint,
		constraints: constraints,
		handler: endpointObject.get
	});

	context.server.route({
		method: "POST",
		url: endpoint,
		constraints: constraints,
		handler: endpointObject.post
	});

	context.server.route({
		method: "PUT",
		url: endpoint,
		constraints: constraints,
		handler: endpointObject.put
	});
}

async function populateViewEndpoint(context: Context, endpoint: string, endpointObject: Endpoint): Promise<void> {
	const constraints = context.environment === "production" ? { host: context.domain } : undefined;

	await context.log?.info(`Populating view endpoint "${endpoint}" with endpointObject ${endpointObject.name}`);

	context.server.route({
		method: "GET",
		url: endpoint,
		constraints: constraints,
		handler: endpointObject.get
	});
}
