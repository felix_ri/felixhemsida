import ejs from "ejs";
import fastifyStatic from "fastify-static";
import path from "path";
import fastifyPointOfView from "point-of-view";

import { Context } from "./types";

export function startServers(context: Context) {
	const address: string = context.environment === "production" ? "::" : "localhost";

	context.server.listen(context.port, address, (error, address) => {
		if (error) {
			throw error;
		}

		context.log?.info(`Server listening on ${address} in environment ${context.environment}`);
	});
}

export function addHomeRedirect(context: Context): void {
	context.server.get("/", async (request, response) => {
		response.redirect("/index/view");
	});
}

export function registerPlugins(context: Context): void {
	context.server.register(fastifyStatic, {
		root: path.join(process.cwd(), "public"),
		prefix: "/",
	});

	context.server.register(fastifyPointOfView, {
		engine: {
			"ejs": ejs
		}
	});
}

export function addHealthCheck(context: Context): void {
	const endpoint = "/health-check";

	context.server.route({
		method: "GET",
		url: endpoint,
		handler: async (request, response) => response.code(200).send()
	});
}
