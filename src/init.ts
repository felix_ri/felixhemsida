import BetterSqlite3 from "better-sqlite3";
import fs from "fs";
import path from "path";

import { getDatabasePath } from "./helpers";
import { Logger } from "./Logger";

async function init(log?: Logger): Promise<void> {
	await initSelfSignedCert(log);

	await initDatabase(log);
}

export async function initSelfSignedCert(log?: Logger): Promise<void> {
	log?.info("Creating self signed cert");

	const selfsigned = require("selfsigned");
	const attrs = [{ name: "commonName", value: "*.ringberg.net" }];
	const pems = selfsigned.generate(attrs, { days: 365 });

	fs.writeFileSync(path.join(process.cwd(), "sslcert", "key.pem"), pems.private);
	fs.writeFileSync(path.join(process.cwd(), "sslcert", "cert.pem"), pems.cert);
}

async function initDatabase(log?: Logger): Promise<void> {
	const dbPath = getDatabasePath();
	await log?.info(`Creating SQLite database file "${dbPath}"`);
	const db = new BetterSqlite3(dbPath, { fileMustExist: false });

	const directory = path.join(process.cwd(), "db/init");
	const fileNames = fs.readdirSync(directory);

	for (const fileName of fileNames) {
		// Only read sql files
		if (fileName.toLowerCase().endsWith(".sql")) {
			const filePath = path.join(directory, fileName);
			const query = fs.readFileSync(filePath).toString();
			await log?.info(`Initializing database from script "${filePath}"`);
			db.prepare(query).run();
		}
	}
}

init(new Logger());
