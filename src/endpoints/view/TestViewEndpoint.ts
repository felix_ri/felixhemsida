import { Logger } from "../../Logger";
import { Endpoint } from "../Endpoint";
import { RequestHandler } from "../types";

export class TestViewEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("TestViewEndpoint", log);
	}

	public get: RequestHandler = async (request, response) => {
		return response.view("/templates/index.ejs");
	}
}
