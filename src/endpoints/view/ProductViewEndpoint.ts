
import { getDatabase } from "../../helpers";
import { Logger } from "../../Logger";
import { Endpoint, Schema } from "../Endpoint";
import { EndpointError } from "../EndpointError";
import { RequestHandler } from "../types";
import { Product } from "../types";
import { populateProductImages } from "./utils";

export class ProductViewEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("ProductViewEndpoint", log);
		const schemas: Array<Schema> = [
			{
				id: "GET",
				type: "object",
				properties: {
					params: {
						type: "object",
						properties: {
							productId: {
								type: "string",
								required: true
							}
						},
						required: true
					}
				}
			}
		];

		this.addSchemas(schemas);
	}

	public get: RequestHandler = async (request, response) => {
		try {
			this.validate("GET", request);
			const params = request.params as any;
			const productId: string = params.productId as string;

			const requestProduct: Product = {
				id: productId
			};

			const database = getDatabase();

			const select = database.prepare("SELECT id, name, price_sek FROM product WHERE id = @id;");
			const product: Product = select.get(requestProduct);

			populateProductImages(product);

			return response.view("/templates/product.ejs", { product, page: "products" });
		} catch (error) {
			this.log?.error(error);
			if (error instanceof EndpointError) {
				this.respond(response, { endpointError: error });
			} else {
				this.respond(response, { code: 500, payload: "Internal server error" });
			}
		}
	}
}
