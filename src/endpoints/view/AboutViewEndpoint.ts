import { Logger } from "../../Logger";
import { Endpoint } from "../Endpoint";
import { RequestHandler } from "../types";

export class AboutViewEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("AboutViewEndpoint", log);
	}

	public get: RequestHandler = async (request, response) => {
		return response.view("/templates/about.ejs", { page: "about" });
	}
}
