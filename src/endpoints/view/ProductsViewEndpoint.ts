import { getDatabase } from "../../helpers";
import { Logger } from "../../Logger";
import { Endpoint } from "../Endpoint";
import { RequestHandler } from "../types";
import { Product } from "../types";
import { populateProductImages } from "./utils";

export class ProductsViewEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("ProductsViewEndpoint", log);
	}

	public get: RequestHandler = async (request, response) => {
		let products: Array<Product> | undefined;
		try {
			const database = getDatabase();

			const select = database.prepare("SELECT id, name, price_sek FROM product;");

			products = select.all();

			populateProductImages(...products);
		} catch (error) {
			await this.log?.error(error);
		}

		return response.view("/templates/products.ejs", { products, page: "products" });
	}


}
