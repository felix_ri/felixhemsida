import { readdirSync } from "fs";
import path from "path";

import { ImageTag, Product } from "../types";

export function populateProductImages(...products: Array<Product>) {
	const productImagesFolder = "img/products";
	const directory = path.join(process.cwd(), "public", productImagesFolder);
	const imageFilePaths = readdirSync(directory);

	imageFilePaths.forEach(imageFilePath => {
		const regex = /^(.*)#(.*)\..*$/;
		const found = imageFilePath.match(regex);
		if (found) {
			const [filepath, productId, imageTag] = found as [string, string, ImageTag, string];

			if (filepath && productId && imageTag) {
				const imageFilePath = `/${productImagesFolder}/${filepath.replace("#", "%23")}`;
				const product = products.find(({ id }) => id === productId);
				if (product) {
					if (!product.images) {
						product.images = {};
					}
					product.images[imageTag] = imageFilePath;
				}
			}
		}
	});
}
