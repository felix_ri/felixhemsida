
import { Endpoint } from "../Endpoint";
import { RequestHandler } from "../types";

export class IndexViewEndpoint extends Endpoint {
	constructor() {
		super("IndexViewEndpoint");
	}

	public get: RequestHandler = async (request, response) => {
		return response.view("/templates/index.ejs", { page: "index" });
	}
}
