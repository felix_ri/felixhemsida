import { FastifyReply } from "fastify";
import { Schema, Validator } from "jsonschema";

import { Logger } from "../Logger";
export { Schema } from "jsonschema";

import { EndpointError } from "./EndpointError";
import { EndpointResponseOptions, IEndpoint, RequestHandler } from "./types";

export abstract class Endpoint implements IEndpoint {
	protected validator: Validator;
	protected log?: Logger;
	readonly name: string;

	constructor(name: string, log?: Logger) {
		this.validator = new Validator();
		this.name = name;
		this.log = log;
	}

	protected addSchemas(schemas: Array<Schema>) {
		for (const schema of schemas) {
			this.validator.addSchema(schema);
		}
	}

	protected validate(schemaId: string, request: any): void {
		const schema = this.validator.schemas["/" + schemaId];
		const result = this.validator.validate(request, schema);

		if (result.valid === false) {
			throw new EndpointError("BADREQUEST");
		}
	}

	protected respond(response: FastifyReply, options?: EndpointResponseOptions) {
		const error = options?.endpointError;
		const code = !error && options?.code ? options.code : 200;
		const payload = options?.payload;
		const type = options?.type ? options.type : "text/html";
		if (error) {
			response.code(error.code);
			response.send(error.message);
		} else {
			response.code(code);
			response.type(type);
			if (payload) {
				response.send(payload);
			}
		}
	}

	public get: RequestHandler = async (request, response) => {
		void request, response;
		throw new EndpointError("METHODNOTIMPLEMENTED");
	}

	public post: RequestHandler = async (request, response) => {
		void request, response;
		throw new EndpointError("METHODNOTIMPLEMENTED");
	}

	public put: RequestHandler = async (request, response) => {
		void request, response;
		throw new EndpointError("METHODNOTIMPLEMENTED");
	}
}
