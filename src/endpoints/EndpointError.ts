import { EndpointErrorType, EndpointResponseCode } from "./types";

export class EndpointError extends Error {
	readonly name = "EndpointError";
	readonly code: EndpointResponseCode;
	constructor(type: EndpointErrorType) {
		super();

		switch (type) {
			case "BADREQUEST": {
				this.message = "Bad request";
				this.code = 400;
				break;
			}
			case "METHODNOTIMPLEMENTED": {
				this.message = "Method not yet implemented";
				this.code = 500;
				break;
			}
			default: {
				this.message = "Internal server error";
				this.code = 500;
				break;
			}
		}
	}
}
