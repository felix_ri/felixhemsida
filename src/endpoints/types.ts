/* eslint-disable no-unused-vars */
import { RouteHandlerMethod } from "fastify";

import { Endpoint } from "./Endpoint";
import { EndpointError } from "./EndpointError";

export interface IEndpoint {
	get: RequestHandler;
	post: RequestHandler;
	put: RequestHandler;
}

export type EndpointMap = {
	[endpoint: string]: Endpoint;
}

export type ImageTag = "front" | "side" | "back";

export type ProductImages = {
	[imageTag in ImageTag]?: string;
};

export type Product = {
	id: string;
	name?: string;
	price_sek?: number;
	images?: ProductImages;
}

export type RequestHandler = RouteHandlerMethod;

export type EndpointResponseOptions = {
	endpointError?: EndpointError;
	code?: EndpointResponseCode;
	payload?: any;
	type?: string;
}

export type EndpointErrorType =
	"BADREQUEST" |
	"METHODNOTIMPLEMENTED";

export type EndpointResponseCode = 200 | 400 | 500;
