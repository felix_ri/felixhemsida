

import { Logger } from "../../Logger";
import { Endpoint } from "../Endpoint";

export class ProductsEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("ProductsEndpoint", log);
	}
}
