
import { Logger } from "../..//Logger";
import { getDatabase } from "../../helpers";
import { Endpoint, Schema } from "../Endpoint";
import { EndpointError } from "../EndpointError";
import { RequestHandler } from "../types";
import { Product } from "../types";

export class ProductEndpoint extends Endpoint {
	constructor(log?: Logger) {
		super("ProductEndpoint", log);
		const schemas: Array<Schema> = [
			{
				id: "PUT",
				type: "object",
				properties: {
					headers: {
						type: "object",
						properties: {
							"content-type": {
								type: "string",
								pattern: "^application/json$",
								required: true
							}
						},
						required: true
					},
					params: {
						type: "object",
						properties: {
							productId: {
								type: "string",
								required: true
							}
						},
						required: true
					},
					body: {
						type: "object",
						properties: {
							name: {
								type: "string",
								required: false
							},
							price_sek: {
								type: "number",
								required: false
							}
						},
						required: true
					}
				}
			}
		];

		this.addSchemas(schemas);
	}

	public get: RequestHandler = async (request, response) => {
		try {
			this.validate("GET", request);
			const params = request.params as any;
			const productId: string = params.productId as string;

			const requestProduct: Product = {
				id: productId
			};

			const database = getDatabase();

			const select = database.prepare("SELECT id, name, price_sek FROM product WHERE id = @id;");
			const product: Product = select.get(requestProduct);

			return response.view("/templates/product.ejs", { product });
		} catch (error) {
			await this.log?.error(error);
			if (error instanceof EndpointError) {
				this.respond(response, { endpointError: error });
			} else {
				this.respond(response, { code: 500, payload: "Internal server error" });
			}
		}
	}

	public put: RequestHandler = async (request, response) => {
		try {
			// this.validate("PUT", request);

			const params = request.params as any;
			const body = request.body as any;
			const product: Product = {
				id: params.productId,
				name: body.name,
				price_sek: body.price_sek
			};

			const database = getDatabase();

			const insert = database.prepare("INSERT OR REPLACE INTO product (id, name, price_sek) VALUES (@id, @name, @price_sek);");

			insert.run(product);
			response.send(product);
		} catch (error) {
			await this.log?.error(error);
			if (error instanceof EndpointError) {
				this.respond(response, { endpointError: error });
			} else {
				this.respond(response, { code: 500, payload: "Internal server error" });
			}
		}
	}
}
