export type LogType = "ERROR" | "INFO" | "DEBUG" | "WARNING";

export type LoggerOptions = {
	logToConsole?: boolean;
	logToFile?: boolean;
	logDebug?: boolean | "FILEONLY" | "CONSOLEONLY";
	logFilenamePrefix?: string;
}

export const defaultOptions: LoggerOptions = {
	logToConsole: true,
	logToFile: false,
	logDebug: false,
	logFilenamePrefix: "log"
};

export type LogMessage = {
	type: LogType;
	time: Date;
	messages: Array<any>
}
