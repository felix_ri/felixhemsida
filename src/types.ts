import { FastifyInstance } from "fastify";

import { Logger } from "./Logger";

export type Context = {
	domain: string;
	apiDomain: string;
	port: number;
	environment: string;
	server: FastifyInstance;
	log?: Logger;
}
