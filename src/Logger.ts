import { promises as fsp } from "fs";
import path from "path";

import { defaultOptions, LoggerOptions, LogType } from "./Logger.types";

export class Logger {
	private options!: LoggerOptions;

	constructor(options?: LoggerOptions) {
		this.setOptions(options);
	}

	private setOptions(options?: LoggerOptions): void {
		this.options = {
			logToConsole: options?.logToConsole || defaultOptions.logToConsole,
			logToFile: options?.logToFile || defaultOptions.logToFile,
			logDebug: options?.logDebug || defaultOptions.logDebug,
			logFilenamePrefix: options?.logFilenamePrefix || defaultOptions.logFilenamePrefix
		};
	}

	public info(...messages: Array<any>): void | Promise<void> {
		return this.log("INFO", messages);
	}

	public error(...messages: Array<any>): void | Promise<void> {
		return this.log("ERROR", messages);
	}

	public debug(...messages: Array<any>): void | Promise<void> {
		return this.log("DEBUG", messages);
	}

	public warning(...messages: Array<any>): void | Promise<void> {
		return this.log("WARNING", messages);
	}

	public log(type: LogType, messages: Array<any>): void | Promise<void> {
		const messagesString = messages
			.map(message => {
				switch (typeof message) {
					case "object": {
						return JSON.stringify(message);
					}
					default: {
						return message;
					}
				}
			})
			.join(" ");

		const logMessage = `${new Date().toISOString()} [${type}] ${messagesString}`;

		if (this.options.logToConsole === true) {
			this.logToConsole(logMessage, type);
		}
		if (this.options.logToFile === true) {
			return this.logToFile(logMessage, type);
		}
	}

	private addZero(number: number): string {
		if (number < 10) {
			return `0${number}`;
		} else {
			return `${number}`;
		}
	}

	private async logToFile(message: string, type: LogType): Promise<void> {
		if (type !== "DEBUG" || this.options.logDebug === true || this.options.logDebug === "FILEONLY") {
			const prefix = this.options.logFilenamePrefix ? this.options.logFilenamePrefix + "-" : "";
			const date = new Date();
			const dateString = `${date.getFullYear()}-${this.addZero(date.getMonth() + 1)}-${this.addZero(date.getDate())}`;
			const filename = `${prefix}${dateString}.log`;
			const filePath = path.join(process.cwd(), "logs", filename);

			await fsp.appendFile(filePath, message + "\n");
		}
	}

	private logToConsole(message: string, type: LogType): void {
		if (this.options.logToConsole === true) {
			switch (type) {
				case "INFO": {
					// eslint-disable-next-line no-console
					console.info(message);
					break;
				}
				case "ERROR": {
					// eslint-disable-next-line no-console
					console.error(message);
					break;
				}
				case "DEBUG": {
					if (this.options.logDebug === true || this.options.logDebug === "CONSOLEONLY") {
						// eslint-disable-next-line no-console
						console.debug(message);
					}
					break;
				}
				case "WARNING": {
					// eslint-disable-next-line no-console
					console.warn(message);
					break;
				}
			}
		}
	}
}
