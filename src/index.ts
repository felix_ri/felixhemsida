import fastify from "fastify";
import fs from "fs";
import path from "path";

import { populateEndpoints } from "./endpoints";
import { getVar } from "./helpers";
import { Logger } from "./Logger";
import { LoggerOptions } from "./Logger.types";
import { addHealthCheck, addHomeRedirect, registerPlugins, startServers } from "./serverUtils";
import { Context } from "./types";

async function init(): Promise<void> {
	const loggerOptions: LoggerOptions = {
		logDebug: true,
		logFilenamePrefix: "index",
		logToConsole: true,
		logToFile: true
	};
	const log = new Logger(loggerOptions);
	const port = +getVar("PORT");
	const environment = getVar("NODE_ENV", "development");
	const domain = getVar("DOMAIN");
	const apiDomain = getVar("API_DOMAIN");

	const context: Context = {
		domain: domain,
		apiDomain: apiDomain,
		port: port,
		environment: environment,
		server: fastify({
			https: {
				cert: fs.readFileSync(path.join(process.cwd(), "sslcert", "cert.pem")),
				key: fs.readFileSync(path.join(process.cwd(), "sslcert", "key.pem"))
			}
		}),
		log: log
	};

	registerPlugins(context);

	addHomeRedirect(context);

	addHealthCheck(context);

	await populateEndpoints(context);

	startServers(context);
}

init();
