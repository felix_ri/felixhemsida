import BetterSqlite3 from "better-sqlite3";
import path from "path";

/**
 * Get environment variable from process.env
 * @param variableName name of variable, e.g. NODE_ENV
 * @param defaultValue if value is missing, default to this value instead
 */
export function getVar(variableName: string, defaultValue?: string): string {
	const variable = process.env[variableName];

	if (variable === undefined || variable === "") {
		if (defaultValue !== undefined) {
			return defaultValue;
		} else {
			throw new Error(`Environment variable "${variableName}" not set.`);
		}
	} else {
		return variable;
	}
}

export function getDatabase(): BetterSqlite3.Database {
	const dbPath = getDatabasePath();
	const db = new BetterSqlite3(dbPath, { fileMustExist: true });

	return db;
}

export function getDatabasePath(): string {
	return path.join(process.cwd(), "db", `${getVar("NODE_ENV", "development")}.db`);
}
